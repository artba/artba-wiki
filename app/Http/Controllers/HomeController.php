<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\Models\State as States;

class HomeController extends Controller
{
    /**
     * generate PDF file from blade view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = States::all();
        
        $data = [
            "states" => $states
        ];

        return view("welcome", $data);
    }

    public function about()
    {

        return view("about");
    }
}