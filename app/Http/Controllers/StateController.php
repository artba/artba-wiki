<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\State as States;
use App\Models\District as Districts;
use App\Models\Airport as Airports;
use App\Models\Highway as Highways;
use App\Models\Freight as Freights;
use App\Models\Water as Water;
use App\Models\Bridge as Bridges;
use App\Models\Bullet as Bullets;
use App\Models\Infrastructure as Infrastructures;
use App\Models\Movement as Movements;
use App\Models\Economic as Economics;
use App\Models\Mobility as Mobility;
use App\Models\HighwayFunding as HighwayFunding;
use App\Models\Transit as Transit;
use App\Models\Safety as Safety;
use App\Models\County as Counties;

class StateController extends Controller
{
    /**
     * generate PDF file from blade view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($state, Request $request)
    {
        $export = Str::contains($request->header('user-agent'), "Headless");
        $districts = Districts::where('state_abbr', $state)->get();
        $state = States::where('state_abbr', $state)->first();
        $airports = Airports::where('name', $state->state)->where('cd', 0)->get();
        $airportsFunding = Airports::stateFunding($state->state);
        $highways = Highways::where('name', $state->state)->where('cd', 0)->get();
        $waters = Water::where('name', $state->state)->where('cd', 0)->get();
        $bridges = Bridges::where('name', $state->state)->where('cd', 0)->get();
        $bullets = Bullets::where('name', $state->state)->where('cd', 0)->orderBy('obs', "ASC")->get();
        $freights = Freights::where('name', $state->state)->where('cd', 0)->get();
        $infrastructures = Infrastructures::where('name', $state->state)->where('cd', 0)->get();
        $movementsChartTwo = Movements::graphTwoState($state->state);
        $movementsChartOne = Movements::graphOneState($state->state);
        $movementsText = Movements::textState($state->state);
        $economicsChartTwo = Economics::graphTwoState($state->state);
        $economicsChartOne = Economics::graphOneState($state->state);
        $economicsText = Economics::textState($state->state);
        $mobilityChartTwo = Mobility::graphTwoState($state->state);
        $mobilityChartOne = Mobility::graphOneState($state->state);
        $mobilityText = Mobility::textState($state->state);
        $highwayFunding = HighwayFunding::stateFunding($state->state);
        $transit = Transit::stateFunding($state->state);
        $safety = Safety::stateSafety($state->state);
        $safetyGraph = Safety::graphOneState($state->state);

        $sections = [
            "Infrastructure Network", "Economic Impacts","Mobility","Goods Movement","Safety","Highway Funding","Transit Funding", "Airport Funding", "More Information"];

        $data = [
            "state" => $state,
            "districts" => $districts,
            "airports" => $airports,
            "airportsFunding" => $airportsFunding,
            "highways" => $highways,
            "waters" => $waters,
            "bridges" => $bridges,
            "bullets" => $bullets,
            "freights" => $freights,
            "highwayFunding" => $highwayFunding,
            "transit" => $transit,
            "safety" => $safety["text"],
            "safetyGraph" => $safetyGraph,
            "in" => $infrastructures,
            "movementsChartTwo" => $movementsChartTwo,
            "movementsChartOne" => $movementsChartOne,
            "movementsText" => $movementsText,
            "economicsChartTwo" => $economicsChartTwo,
            "economicsChartOne" => $economicsChartOne,
            "economicsText" => $economicsText,
            "mobilityChartTwo" => $mobilityChartTwo,
            "mobilityChartOne" => $mobilityChartOne,
            "mobilityText" => $mobilityText,
            "airportTotal" => 0,
            "waterTotal" => 0,
            "sections" => $sections,
            "export" => $export
        ];
        
        return view('state', $data);
    }

    public function district($state, $district, Request $request)
    {
        $export = Str::contains($request->header('user-agent'), "Headless");
        $districts = Districts::where('state_abbr', $state)->get();
        $district = Districts::where('state_abbr', $state)->where('district', $district)->first();
        $state = States::where('state_abbr', $state)->first();
        $highways = Highways::where('name', $state->state)->where('cd', $district->district)->get();
        $airports = Airports::where('name', $state->state)->where('cd', $district->district)->get();
        $airportsFunding = Airports::districtFunding($state->state, $district->district);
        $waters = Water::where('name', $state->state)->where('cd', $district->district)->get();
        $bridges = Bridges::where('name', $state->state)->where('cd', $district->district)->get();
        $bullets = Bullets::where('name', $state->state)->where('cd', $district->district)->orderBy('obs', "ASC")->get();
        $freights = Freights::where('name', $state->state)->where('cd', $district->district)->get();
        $infrastructures = Infrastructures::where('name', $state->state)->where('cd', $district->district)->get();
        $movementsChartOne = Movements::graphOneDistrict($state->state, $district->district);
        $movementsChartTwo = Movements::graphTwoDistrict($state->state, $district->district);
        $movementsText = Movements::textDistrict($state->state, $district->district);
        $economicsChartTwo = Economics::graphTwoDistrict($state->state, $district->district);
        $economicsChartOne = Economics::graphOneDistrict($state->state, $district->district);
        $economicsText = Economics::textDistrict($state->state, $district->district);
        $mobilityChartTwo = Mobility::graphTwoDistrict($state->state, $district->district);
        $mobilityChartOne = Mobility::graphOneDistrict($state->state, $district->district);
        $mobilityText = Mobility::textDistrict($state->state, $district->district);
        $highwayFunding = HighwayFunding::districtFunding($state->state, $district->district);
        $transit = Transit::districtFunding($state->state, $district->district);
        $safety = Safety::districtSafety($state->state, $district->district);
        $counties = Counties::excerpt($state->state, $district->district);
        $safetyGraph = Safety::graphOneDistrict($state->state,$district->district);

        $sections = [
            "Infrastructure Network","Economic Impacts","Mobility","Goods Movement","Safety","Highway Funding","Transit Funding", "Airport Funding"];

    
        $data = [
            "state" => $state,
            "districts" => $districts,
            "currentDistrict" => $district,
            "airports" => $airports,
            "airportsFunding" => $airportsFunding,
            "highways" => $highways,
            "waters" => $waters,
            "bridges" => $bridges,
            "bullets" => $bullets,
            "freights" => $freights,
            "transit" => $transit,
            "safety" => $safety["text"],
            "safetyGraph" => $safetyGraph,
            "highwayFunding" => $highwayFunding,
            "in" => $infrastructures,
            "movementsChartOne" => $movementsChartOne,
            "movementsChartTwo" => $movementsChartTwo,
            "movementsText" => $movementsText,
            "economicsChartTwo" => $economicsChartTwo,
            "economicsChartOne" => $economicsChartOne,
            "economicsText" => $economicsText,
            "mobilityChartTwo" => $mobilityChartTwo,
            "mobilityChartOne" => $mobilityChartOne,
            "mobilityText" => $mobilityText,
            "airportTotal" => 0,
            "waterTotal" => 0,
            "sections" => $sections,
            "export" => $export,
            "counties" => $counties
        ];
        
        return view('district', $data);
    }
}