<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Airport extends Model
{
    use HasFactory;

    protected $table= 'airports';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }

    public static function stateFunding($state){
        $funding = [
            "text" => DB::table("airports_text")->where('name', $state)->where('cd', 0)->get(),
            "graph1" => DB::table("airports_graph1")->where('name', $state)->where('cd', 0)->get(),
            "graph2" => DB::table("airports_graph2")->where('name', $state)->where('cd', 0)->get()
        ];

        return $funding;
    }

    public static function districtFunding($state, $cd){
        $funding = [
            "text" => DB::table("airports_text")->where('name', $state)->where('cd', $cd)->get(),
            "graph1" => DB::table("airports_graph1")->where('name', $state)->where('cd', $cd)->get(),
            "graph2" => DB::table("airports_graph2")->where('name', $state)->where('cd', $cd)->get()
        ];

        return $funding;
    }
}
