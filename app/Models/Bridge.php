<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bridge extends Model
{
    use HasFactory;

    protected $table= 'bridges';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
