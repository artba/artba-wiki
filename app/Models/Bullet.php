<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bullet extends Model
{
    use HasFactory;

    protected $table= 'text';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
