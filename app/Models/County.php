<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class County extends Model
{
    use HasFactory;


    public static function excerpt($state, $cd) {
        $countyData = DB::table("counties")->where('state', $state)->where('cd', $cd)->first();

        return "$countyData->text $countyData->text2 $countyData->text3 $countyData->text4 $countyData->text5";
    }
}
