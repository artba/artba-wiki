<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Economic extends Model
{
    use HasFactory;

    

    public static function graphOneState($state){
        return DB::table("econ_graph1")->where("name", $state)->where('cd', 0)->get();
    }

    public static function graphTwoState($state) {
        return DB::table("econ_graph2")->where('name', $state)->where('cd', 0)->get();
    }

    public static function textState($state){
        return DB::table("econ_text")->where('name', $state)->where('cd', 0)->get();
    }

    public static function graphTwoDistrict($state, $cd) {
        return DB::table("econ_graph2")->where('name', $state)->where('cd', $cd)->get();
    }

    public static function graphOneDistrict($state, $cd){
        return DB::table("econ_graph1")->where('name', $state)->where('cd', $cd)->get();
    }

    public static function textDistrict($state, $cd){
        return DB::table("econ_text")->where('name', $state)->where('cd', $cd)->get();
    }
    
}
