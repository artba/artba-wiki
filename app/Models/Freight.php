<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Freight extends Model
{
    use HasFactory;
    
    protected $table= 'freight_transit';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }

}
