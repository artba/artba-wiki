<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Highway extends Model
{
    use HasFactory;

    protected $table= 'highways';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
