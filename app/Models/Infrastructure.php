<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Infrastructure extends Model
{
    use HasFactory;

    protected $table= 'wiki_long_table';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
