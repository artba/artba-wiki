<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Long extends Model
{
    use HasFactory;

    protected $table = 'long_form';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
