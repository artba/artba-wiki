<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Safety extends Model
{
    use HasFactory;

    public static function stateSafety($state){
        $safety = [
            "text" => DB::table("safety_text")->where('name', $state)->where('cd', 0)->get()
        ];

        return $safety;
    }

    public static function districtSafety($state, $cd){
        $safety = [
            "text" => DB::table("safety_text")->where('name', $state)->where('cd', $cd)->get()
        ];

        return $safety;
    }

    public static function graphOneState($state){
        return DB::table("safety_graph")->where("name", $state)->where('cd', 0)->get();
    }

    public static function graphOneDistrict($state, $cd){
        return DB::table("safety_graph")->where('name', $state)->where('cd', $cd)->get();
    }
}
