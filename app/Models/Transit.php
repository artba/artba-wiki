<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Transit extends Model
{
    use HasFactory;

    public static function stateFunding($state){
        $funding = [
            "text" => DB::table("transit_text")->where('name', $state)->where('cd', 0)->get(),
            "graph1" => DB::table("transit_graph1")->where('name', $state)->where('cd', 0)->get(),
            "graph2" => DB::table("transit_graph2")->where('name', $state)->where('cd', 0)->get()
        ];

        return $funding;
    }

    public static function districtFunding($state, $cd){
        $funding = [
            "text" => DB::table("transit_text")->where('name', $state)->where('cd', $cd)->get(),
            "graph1" => DB::table("transit_graph1")->where('name', $state)->where('cd', $cd)->get(),
            "graph2" => DB::table("transit_graph2")->where('name', $state)->where('cd', $cd)->get()
        ];

        return $funding;
    }
}
