<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Water extends Model
{
    use HasFactory;

    protected $table = 'water';

    public function state() {
        return $this->belongsTo('\App\Models\State', 'name', 'state');
    }
}
