<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateHighwayFundingUrls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('hwyfunding_text')
            ->where('text', 'like', '%artba.org/economics/highway-dashboard-iija%')
            ->update([
                'text' => DB::raw('REPLACE(text, 
                    "artba.org/economics/highway-dashboard-iija", 
                    "artba.org/market-intelligence/highway-dashboard-iija"
                )')
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('hwyfunding_text')
            ->where('text', 'like', '%artba.org/market-intelligence/highway-dashboard-iija%')
            ->update([
                'text' => DB::raw('REPLACE(text, 
                    "artba.org/market-intelligence/highway-dashboard-iija", 
                    "artba.org/economics/highway-dashboard-iija"
                )')
            ]);
    }
}
