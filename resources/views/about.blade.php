@extends('layouts.master')

@section('header')
    <title>TranspoInfo - America's Infrastructure At-A-Glance | About</title>
    <meta property="og:url" content="https://transpoinfo.org/about" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="TranspoInfo - America's Infrastructure At-A-Glance | About" />
    <meta property="og:description" content="Key facts about each state’s roads, bridges, waterways and other transportation infrastructure." />
    <meta property="og:image" content="https://transpoinfo.org/img/socialbanner.jpeg" />
    <style type="text/css">
        .kbDoc-banner-support {
            padding: 100px 0;
            background-color: rgb(255, 255, 255) !important;
            background-image: none !important;
        }
        .kbDoc-banner-support .banner-content-wrapper p{
            margin-bottom: 10px !important;
            color: black;
        }
    </style>
@endsection

@section('content')
<section class="kbDoc-banner-support" style="height: inherit !important; backround">
    <div class="banner-content text-center">
        <div class="banner-content-wrapper">
            <h1 class="banner-title ">About</h1>
            <p class="banner-description  text-left" data-wow-delay="0.3s">
                TranspoInfo is a comprehensive profile of transportation systems across the United States. The tool was designed to provide detail on the scope, and funding and economic impact of the networks that safely and efficiently move people and goods across the country. This resource demonstrates that a modern and well-maintained system of roads, bridges, airports and railways is the backbone of our economy. The partnership between federal, state and local governments is key to its success.
            </p>
<p class=" text-left" data-wow-delay="0.3s">
Using this tool, you can:
</p>
<ul class="text-left" data-wow-delay="0.3s">
<li>Learn the scope of transportation systems in your state or congressional district.</li>
<li>See how your state receives and deploys federal funding for transportation projects.</li>
<li>Explore how transportation employs jobs in each state directly and indirectly.</li>
</ul>
<p class="banner-description  text-left">This site is publicly available and is designed with congressional offices in mind. Understanding the transportation make-up of your state or district is the first step toward ensuring it’s well-maintained, adequately funded and meets the needs of constituents.</p>
<p class="banner-description  text-left" data-wow-delay="0.3s">
Data is assembled from a range of publicly available resources, including census data and other federal government resources, and is compiled and updated on an annual basis by ARTBA’s economics staff.
<br><br>
With questions about the data or suggestions, contact <a href="mailto:governmentaffairs@artba.org">governmentaffairs@artba.org</a>.
<br><br>
<strong class="text-left">About ARTBA</strong>
</p>
<p class="banner-description  text-left" data-wow-delay="0.3s">
The Washington, D.C.-based American Road & Transportation Builders Association (ARTBA) brings together all facets of the transportation construction industry to responsibly advocate for infrastructure investment and policy that meet the nation’s need for safe and efficient travel. ARTBA was established in 1902 and seeks to enable a dynamic transportation network that enriches American life.
</p>
            </p>
        </div>
    </div>
</section>
@endsection

@section('js')

@endsection