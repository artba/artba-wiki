 <article class="documentation_body shortcode_text doc-section" id="airport-funding">
     <div class="shortcode_title">
        <h2>Airport Funding</h2>
        <ul>
            @foreach ($airportsFunding["text"] as $airportText)
                <li>{{ $airportText->text }}</li>
            @endforeach
        </ul>
    </div>
    

     <div class="row">
        <div class="col-md-12 ">
            <div class="float-left" style="width: 50%;">
                <div id="airportOnePieChart" class="piechart" style="width: 100%; height: 500px;"></div>
            </div>
            <div class="float-right" style="width: 50%;">
                <div id="airportTwoPieChart" class="piechart" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
    </div>
    <div class="border_bottom"></div>
     <div class="border_bottom"></div>
 </article>
