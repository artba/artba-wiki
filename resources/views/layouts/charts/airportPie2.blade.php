<script type="text/javascript">
    Highcharts.chart('airportTwoPieChart', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Major Airport Expenditures'
        },
        series: [{
            data: [
                @foreach ($points as $point)
                    {
                        name: '{{ str_replace('&', 'and', $point->label) }}',
                        y: {{ round($point->value * 100, 1) }},
                        color: ['rgb(245, 155, 50)',
                            'rgb(55, 25, 186)',
                            'rgb(155, 25, 156)'
                        ][{{ $loop->index }} % 4]
                    },
                @endforeach
            ]
        }],
        tooltip: {
            pointFormat: 'Percentage: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    format: '{point.name} {point.percentage:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        }
    });
</script>
