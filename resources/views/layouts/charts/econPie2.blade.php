<script type="text/javascript">
    var data = [
        @foreach ($points as $point)
            {
                name: '{{ str_replace('&', 'and', $point->label) }}',
                y: {{ round($point->value * 100, 1) }}
            },
        @endforeach
    ];

    Highcharts.chart('economicPieChartTwo', {
        exporting: {
            enabled: false
        },
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Types of Jobs Supported'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                colors: [
                    'rgb(245, 155, 50)',
                    'rgb(55, 25, 186)',
                    'rgb(55, 225, 86)',
                    'rgb(155, 25, 156)'
                ]
            }
        },
        series: [{
            name: 'Percentage',
            colorByPoint: true,
            data: data
        }],
        credits: {
            enabled: false
        }
    });
</script>
