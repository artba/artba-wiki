<script type="text/javascript">
    const goodsMovementBarlabels = [
        @foreach ($points as $point)
            "{{ $point->label }}",
        @endforeach
    ];
    Highcharts.chart('goodsMovementBarChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Value of Freight Shipments in 2022'
        },
        xAxis: {
            categories: goodsMovementBarlabels
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        series: [{
            name: "Value in Billions",
            data: [
                @foreach ($points as $point)
                    {{ $point->value }},
                @endforeach
            ],
            color: 'rgb(55, 25, 186)'
        }],
        tooltip: {
            valueSuffix: " Billions"
        },
      plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Highcharts.numberFormat(this.y, 2, '.', ',') + 'B';
                    }
                }
            }
        },

        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        }
    });
</script>
