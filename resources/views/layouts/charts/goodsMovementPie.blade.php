<script type="text/javascript">
    Highcharts.chart('goodsMovementPieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Value of Freight Shipments'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                colors: [
                    'rgb(255, 99, 132)',
                    'rgb(54, 162, 235)',
                    'rgb(255, 205, 86)',
                    'rgb(245, 155, 50)',
                    'rgb(55, 25, 186)',
                    'rgb(55, 225, 86)',
                    'rgb(155, 25, 156)'
                ],
                // showInLegend: true

            },
        },
        series: [{
            name: 'Percentage',
            data: [
                @foreach ($points as $point)
                    {
                        name: '{{ str_replace('&', 'and', $point->label) }}',
                        y: {{ $point->value }}
                    },
                @endforeach
            ]
        }],
        credits: {
            enabled: false
        }
    });
</script>
