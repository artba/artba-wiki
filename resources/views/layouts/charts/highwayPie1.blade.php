<script type="text/javascript">

Highcharts.chart('highwayOnePieChart', {
  chart: {
    type: 'pie'
  },
  title: {
    text: "{{ ($state->state == 'District of Columbia') ? 'District' : 'State'}} {{ 'Highway Program Revenues' }}"
  },
   tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        format: '{point.name}: {point.y:.1f}%'
      }
    }
  },
    credits: {
    enabled: false
  },
  exporting: {
    enabled: false
  },
  series: [{
    name: "{{ ($state->state == 'District of Columbia') ? 'District' : 'State'}} {{ 'Highway Program Revenues' }}",
    data: [
      @foreach($points as $point)
        { name: '{{ str_replace('&','and',$point->label) }}', y: {{ round($point->value * 100,1) }} },
      @endforeach
    ],
    colors: [
      'rgb(245, 155, 50)',
      'rgb(55, 25, 186)',
      'rgb(55, 225, 86)'
    ]
  }]
});

</script>