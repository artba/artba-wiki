<script type="text/javascript">
// const highwayThreeDataPie = {
//   labels: [
//     @foreach($points as $point)
//         '{{ str_replace('&','and',$point->label) }}',
//     @endforeach
//   ],
//   datasets: [{
//     label: "Local Govt. Highway Program Revenues",
//     data: [
//       @foreach($points as $point)
//         {{ round($point->value * 100,1) }},
//       @endforeach
//     ],
//     backgroundColor: [
//       'rgb(245, 155, 50)',
//       'rgb(55, 25, 186)',
//       'rgb(55, 225, 86)'
//     ],
//     hoverOffset: 4,
//     tooltip: {
//       callbacks: {
//         label: function(context) {
//           return context.label + ": " + context.formattedValue + "%"
//         }
//       }
//     }
//   }]
// };

//   const highwayThreePieConfig = {
//   type: 'pie',
//   data: highwayThreeDataPie,
//   options: {
//         responsive: true,
        
//     },
// };

//   const highwayThreePieChart = new Chart(
//     document.getElementById('highwayThreePieChart'), highwayThreePieConfig
//   );
// Highcharts version of the above:
Highcharts.chart('highwayThreePieChart', { 
  chart: {
    type: 'pie',
  },
  title: {
    text: 'Local Govt. Highway Program Revenues'
  },
  series: [{
    name: "Local Govt. Highway Program Revenues",
    data: [
      @foreach($points as $point)
        { name: '{{ str_replace("&", "and", $point->label) }}', y: {{ round($point->value * 100, 1) }},},
      @endforeach
    ],
     colors: [
      'rgb(245, 155, 50)',
      'rgb(55, 25, 186)',
      'rgb(55, 225, 86)'
    ],
    dataLabels: {
      format: '{point.name}: {point.y:.1f}%'
    },
    tooltip: {
      pointFormat: '{point.name}: {point.y:.1f}%'
    }
  }],
  credits: {
    enabled: false
  }
});
</script>