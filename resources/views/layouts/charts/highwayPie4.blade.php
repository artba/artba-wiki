<script type="text/javascript">
const highwayFourDataPie = {
  labels: [
    @foreach($points as $point)
        '{{ str_replace('&','and',$point->label) }}',
    @endforeach
  ],
  datasets: [{
    label: "Local Govt. Highway Program Spending",
    data: [
      @foreach($points as $point)
        {{ round($point->value * 100,1) }},
      @endforeach
    ],
    backgroundColor: [
      'rgb(245, 155, 50)',
      'rgb(55, 25, 186)',
      'rgb(55, 225, 86)'
    ],
    hoverOffset: 4,
    tooltip: {
      callbacks: {
        label: function(context) {
          return context.label + ": " + context.formattedValue + "%"
        }
      }
    }
  }]
};

  const highwayFourPieConfig = {
  type: 'pie',
  data: highwayFourDataPie,
  options: {
        responsive: true,
        
    },
};

  const highwayFourPieChart = new Chart(
    document.getElementById('highwayFourPieChart'), highwayFourPieConfig
  );
</script>