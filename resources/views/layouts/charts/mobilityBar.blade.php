<script type="text/javascript">
    var mobilityBarlabels = [
        @foreach ($points as $point)
            "{{ $point->label }}",
        @endforeach
    ];

    var mobilityDataBar = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'One-Way Commuting Times'
        },
        xAxis: {
            categories: mobilityBarlabels
        },
        yAxis: {
            title: {
                text: 'One-Way Commuting Times'
            },
            labels: {
                format: '{value}%'
            }
        },
        exporting: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
         plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}%'

                }
            }
        },
        series: [{
            name: 'One-Way Commuting Times',
            data: [
                @foreach ($points as $point)
                    {{ $point->value * 100, 1 }},
                @endforeach
            ],
            color: 'rgb(55, 225, 86)'
        }]
    };

    Highcharts.chart('mobilityBarChart', mobilityDataBar);
</script>
