<script type="text/javascript">
    var mobilityDataPie = {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'How Employees Get to Work'
        },
        series: [{
            name: 'Percentage',
            colorByPoint: true,
            data: [
                @foreach ($points as $point)
                    {
                        name: '{{ str_replace('&', 'and', $point->label) }}',
                        y: {{ round($point->value * 100, 1) }}
                    },
                @endforeach
            ]

        }],
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                colors: [
                    'rgb(255, 99, 132)',
                    'rgb(54, 162, 235)',
                    'rgb(255, 205, 86)',
                    'rgb(245, 155, 50)',
                    'rgb(55, 25, 186)',
                    'rgb(55, 225, 86)',
                    'rgb(155, 25, 156)'
                ]
            }
        },
    };

    Highcharts.chart('mobilityPieChart', mobilityDataPie);
</script>
