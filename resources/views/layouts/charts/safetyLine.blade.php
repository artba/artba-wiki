<script type="text/javascript">
    var mobilityBarlabels = [        @foreach ($points as $point)            "{{ $point->label }}",        @endforeach    ];

    var mobilityDataBar = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Highway Fatalities'
        },
        xAxis: {
            categories: mobilityBarlabels
        },
        yAxis: {
            title: {
                text: 'Fatalities'
            },
            labels: {
                format: '{value}'
            }
        },
        exporting: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:,.0f}</b>'
        },
        credits: {
            enabled: false
        },
        //show values on top of points
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Highcharts.numberFormat(this.y, 0, '.', ',');
                    }
                }
            }
        },
        series: [{
            name: 'Fatalities',
            data: [
                @foreach ($points as $point)
                    {{ $point->value  }},
                @endforeach
            ],
            color: 'rgb(220,54,69)'
        }]
    };

    Highcharts.chart('saftyLineChart', mobilityDataBar);

// Convert it into line chart 
</script>
