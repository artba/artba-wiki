<article class="documentation_body shortcode_text doc-section" id="highway-funding">
    <div class="shortcode_title">
        <h2>Highway Funding</h2>
        @if (isset($currentDistrict))
            <i>This is state-level data.</i>
            <br><br>
        @endif
        <ul>
            @php
                $i = 0;
            @endphp
            @foreach ($highwayFunding['text'] as $highwayFundingText)
                <li>{{ $highwayFundingText->text }}
                    @if (!isset($currentDistrict))
                        @if ($i == 1)
                            <a href="https://artbabridgereport.org/state/profile/{{ $state['state_abbr'] }}"
                                style="color:blue;" target="_blank">Learn More: Bridge Conditions</a>
                        @endif
                    @else
                        @if ($i == 1)
                            <a href="https://artbabridgereport.org/congressional/district/{{ $state['state_abbr'] }}/{{ $currentDistrict->district }}"
                                style="color:blue;" target="_blank">Learn More: Bridge Conditions</a>
                        @endif
                    @endif
                    @if (!isset($currentDistrict))
                        @if ($i == 0)
                            <a href="https://www.artba.org/market-intelligence/highway-dashboard-iija/federal-highway-program-impact-iija-state/?state={{ $state['state'] }}"
                                style="color:blue;" target="_blank">See the Top Federal-Aid Highway Projects</a>
                        @endif
                    @else
                        @if ($i == 0)
                            <a href="https://www.artba.org/market-intelligence/highway-dashboard-iija/federal-highway-program-impact-iija-district-state/?state={{ $state['state'] }}&dist={{ $state['state'] }}%20District%20{{ $currentDistrict->district }}"
                                style="color:blue;" target="_blank">See the Top Federal-Aid Highway Projects</a>
                        @endif
                    @endif
                    @php
                        $i++;
                    @endphp
            @endforeach
        </ul>
    </div>
 


    <div class="row">
        <div class="col-md-12 ">
            <div class="float-left" style="width: 50%;">
                <div id="highwayOnePieChart" class="piechart" style="width: 100%; height: 500px;"></div>
            </div>
            <div class="float-right" style="width: 50%;">
                <div id="highwayTwoPieChart" class="piechart" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
    </div>
    <div class="border_bottom"></div>
</article>
