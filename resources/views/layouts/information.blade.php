<article class="documentation_body doc-section pt-5" id="more-information" >
    <div class="shortcode_title">
        @if(isset($currentDistrict))
        <p>{{ $counties }}</p>
        @endif
        <h3> For More Information:</h3>
        <p>Please contact <a href="mailto:governmentaffairs@artba.org">governmentaffairs@artba.org</a>.
        </p>
        <table>
        </table>
    </div>
</article>