<div class="row">
    <div class="col-md-12" style="margin-bottom: 75px">
        <h2>Scope of Infrastructure Network</h2>
    </div>
    <div class="col-md-6">
        <table class="table basic_table_info">
            <tbody>
                <tr class="category">
                    <td colspan="2"><p><strong>Highways, Roads & Bridges</strong></p></td>
                </tr>
                <tr>
                    <td class="">Total Road Mileage</td>
                    @if($in->where('type', 'Total Rural Miles')->first() !== null && $in->where('type', 'Total Urban Miles')->first() !== null)
                    <td class="text-right">
                        {{ number_format($in->where('type', 'Total Rural Miles')->first()->value + $in->where('type', 'Total Urban Miles')->first()->value) }}
                    </td>
                    @endif
                </tr>
                @foreach ($highways as $highway)
                    <tr>
                        <td class="indent">{{ $highway->type }}</td>
                        <td class="text-right">{{ number_format($highway->value) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Number of Bridges</td>
                    <td class="text-right">
                        {{ number_format($in->where('type', 'Bridges')->first()->value) }}
                    </td>
                </tr>
                <tr class="category">
                    <td colspan="2"><strong>Transit & Rail</strong></td>
                </tr>
                @foreach ($freights as $freight)
                    <tr>
                        <td>{{ $freight->type }}</td>
                        <td class="text-right">{{ number_format($freight->value) }}</td>
                    </tr>
                @endforeach()
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table">
            <tbody>
                <tr class="category">
                    <td colspan="2"><strong>Airports</strong></td>
                </tr>
                @foreach ($airports as $airport)
                    @php
                        $airportTotal += $airport->value;
                    @endphp
                    <tr>
                        <td>{{ $airport->type }}</td>
                        <td class="text-right">{{ number_format($airport->value) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Number of Airports</td>
                    <td class="text-right">{{ number_format($airportTotal) }}</td>
                </tr>
                <tr class="category">
                    <td colspan="2"><strong>Ports & Waterways</strong></td>
                </tr>
                @foreach ($waters as $water)
                    @php
                        $waterTotal += $water->value;
                    @endphp
                    <tr>
                        <td>{{ $water->type }}</td>
                        <td class="text-right">{{ number_format($water->value) }}</td>
                    </tr>
                @endforeach
                <tr class="total">
                    <td>Number of Waterways</td>
                    <td class="text-right">{{ number_format($waterTotal) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
