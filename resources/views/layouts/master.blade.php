<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/slick/slick.css">
    <link rel="stylesheet" href="/assets/slick/slick-theme.css">
    <!-- icon css-->
    <link rel="stylesheet" href="/css/eleganticon/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <link rel="stylesheet" href="/assets/animation/animate.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
    <style type="text/css">
        .kbDoc-banner-support .banner-content-wrapper{
            max-width: 1080px;
        }
        
    </style>
    @yield('header')
</head>

<body data-scroll-animation="true">

    <div class="body_wrapper" id="wrapper">
        <div class="overlay" style="display: none;"></div>
        @include('layouts.nav')

        @yield('content')

        <footer class="footer_area f_bg_color">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <h4>Terms of Use</h4>
                            <p>Any use of ARTBA’s data beyond research and related work must be approved by ARTBA beforehand. All use of ARTBA’s data and reports must include proper credit to "American Road & Transportation Builders Association."</p>
                        </div>
                    </div>
                    <div class="border_bottom"></div>
                </div>
            </div>
            <div class="footer_bottom text-center">
                <div class="container">
                    <p>Copyright © {{ Date('Y') }} <a href="https://www.artba.org">American Road & Transportation Builders Association</a></p>
                </div>
            </div>
        </footer>
    </div>

    <!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/pre-loader.js"></script>
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/parallaxie.js"></script>
    <script src="/js/TweenMax.min.js"></script>
    <script src="/js/jquery.parallax-scroll.js"></script>
    <script src="/assets/wow/wow.min.js"></script>
    <script src="/assets/counterup/jquery.counterup.min.js"></script>
    <script src="/assets/counterup/jquery.waypoints.min.js"></script>
    <script src="/assets/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/assets/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-SEK9CXEZEG"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-SEK9CXEZEG'); </script>
    <script src="/js/main.js"></script>

    <script src="/Map/raphael.min.js"></script>
    <script src="/Map/settings.js"></script>
    <script src="/Map/paths.js"></script>
    <script src="/Map/map.js"></script>

    @yield('js')
    
    <script type="text/javascript">
    jQuery(document).ready(function ($) {
          var trigger = $('.burger'),
              overlay = $('.overlay'),
             isClosed = false;
        
            trigger.click(function () {
              burger_cross();      
              $('#wrapper').toggleClass('toggled');
            });

            function burger_cross() {
              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
                
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }

          $(".burger").click(function(){
            $('#wrapper').toggleClass('toggled');
          });
  
        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper').toggleClass('toggled');
        });
        
        $('body').on("click", ".navbar-toggle", function () {
           $(".primarymenu").toggleClass('collapse');
            console.log("show" );
        });

        $.get( "https://www.artba.org/wp-content/themes/Avada/home/shelf.php?site=bridge", function( data ) {
          $( "#menu-artba-menu-3" ).html( data );
        });
    });
</script>

</html>
