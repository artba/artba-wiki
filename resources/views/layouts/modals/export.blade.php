<style type="text/css">
    .modal-backdrop {
        background-color: rgba(0, 0, 0, 0.4);
        z-index: 1040 !important;
    }

    .modal-content {
        margin: 2px auto;
        z-index: 1100 !important;
    }

    .modal-title {
        color: #000 !important;
    }

    .modal-body {
        color: #000 !important;
        font-size: 14px !important font-weight: 300 !important
    }
</style>
<div class="modal" tabindex="-1" role="dialog" id="formModal" style="margin-top:5%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Export Options</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="card-body">
                    <table id="tblPDF">
                        <tr>
                            <td>
                                <input id="all-sections" class="sectionSelectorAll" type="checkbox" checked="checked"
                                    value="all" />
                                <label for="all-sections">All Sections</label>
                            </td>
                        </tr>
                        @for ($pg = 1; $pg <= sizeof($sections); $pg++)
                            <tr>
                                <td>
                                    <input class="sectionSelector" id="section-{{ $pg }}" type="checkbox"
                                        value="{{ $pg }}" />
                                    <label for="section-{{ $pg }}">{{ $sections[$pg - 1] }}</label>
                                </td>
                            </tr>
                        @endfor
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-next" id="next-page">Next</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="formModal2" style="margin-top:5%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Email PDF</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="email-form">
                    <div class="form-group row">
                        <label for="email-input" class="col-12">Email Address</label>
                        <div class="col-12">
                            <input type="email" class="form-control" id="email-input" required>
                            <div class="invalid-feedback">Please provide a valid email address.</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-6">
                            <label for="name-input">Name</label>
                            <input type="text" class="form-control" id="name-input">
                        </div>
                        <div class="col-6">
                            <label for="job-title-input">Job Title</label>
                            <input type="text" class="form-control" id="job-title-input">
                        </div>
                    </div>
                </form>
                <div class="disclaimer">Information not for marketing purposes</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-prev" id="prev-page">Previous</button>
                <button type="button" class="btn btn-primary" id="submit-form">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="formModal3" style="margin-top:5%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Submission</h5>
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div id="loading-spinner"></div>
                <div id="form-message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    $(document).ready(function() {

        let city, region, country, ip;
        var district = 0;
  var currentDistrict = "<?php echo isset($currentDistrict->district) ? $currentDistrict->district : 0; ?>";


        try {
            fetch('https://ipapi.co/json/')
                .then(response => response.json())
                .then(data => {
                    city = data.city;
                    region = data.region;
                    country = data.country_name;
                    ip = data.ip;
                    console.log(
                        // `Your approximate location is: ${data.city}, ${data.region}, ${data.country_name} and your IP address is ${data.ip}`
                    );
                });
        } catch (error) {
            console.log(error);

        }



        // handle next button click
        $('#next-page').on('click', function() {
            var selected = $(".sectionSelector:checked").map(function() {
                return this.value;
            }).get().join(',');
            // store the selected sections in data-* attribute
            $('#formModal2').attr('data-selected-sections', selected);
            // show the next modal
            $('#formModal').modal('hide');
            $('#formModal2').modal('show');
        });

        // handle prev button click
        $('#prev-page').on('click', function() {
            $('#formModal2').modal('hide');
            $('#formModal').modal('show');
        });

        $('#submit-form').on('click', function(e) {

            // prevent form submission
            e.preventDefault();
            // validate email input
            let emailInput = document.querySelector("#email-input");
            if (!emailInput.checkValidity()) {
                emailInput.classList.add("is-invalid");
                return;
            } else {
                emailInput.classList.remove("is-invalid");
            }
            var pathname = window.location.pathname;
            var subjectLine = "{{ $state->state }}" + "Transportation Facts";
            var filename = "transportation_facts_" + subjectLine + ".pdf";
            var device = window.innerWidth < 768 ? "mobile" : "desktop";
            var sections = [
                "Infrastructure Network", "Economic Impacts", "Mobility", "Goods Movement",
                "Safety", "Highway Funding", "Transit Funding", "Airport Funding",
                "More Information"
            ];
            var num = $("#formModal2").data("selected-sections");
            //extract company name from email and conver it to title case

            var company = $("#email-input").val().split("@")[1].split(".")[0];


            // convert pg numbers to the names of the sections string if number is 0 assign  All Sections

            try {
                if (num == 0) {
                    var selectedsections = "All Sections";
                } else {
                    var selectedsections = num.toString().split(",").map(function(item) {
                        return sections[item - 1];
                    }).join(", ");
                }
            } catch (error) {
                console.log('error', error);
            }




            // gather form data
            let formData = {
                email: $("#email-input").val(),
                name: $("#name-input").val(),
                jobtitle: $("#job-title-input").val(),
                pg: $("#formModal2").data("selected-sections"),
                url: "https://transpoinfo.org" + pathname,
                bodyText: "Attached you will find a PDF export of transportation facts from our website",
                subjectLine: subjectLine,
                filename: filename,
                selectedsections: selectedsections,
                pathname: "{{ $state->state }}",
                district: currentDistrict,
                company: company,
                device: device,
                city: city,
                region: region,
                country: country,
                ip: ip
            };
            $("#formModal3").modal("show");
            $("#formModal2").modal("hide");
            $("#loading-spinner").show();
            $("#loading-spinner").html(
                '<img src="https://www.artba.org/wp-content/uploads/2023/01/loader.gif" alt="Loading..." width="50px">'
            ).show();
            $("#form-message").text("");




            //build URL parameter
            let urlParams = Object.keys(formData).map(function(key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(formData[key]);
            }).join('&');

            // send POST request
            axios({
                    method: 'post',
                    url: 'https://b4qyaiouscte5mtek47e3piaaq0hdnls.lambda-url.us-east-1.on.aws/?' +
                        urlParams,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
                .then(response => {
                    if (response.data.message === 'Success') {
                        console.log('Success!', urlParams);
                        $("#form-message").text("PDF has been sent to your email address!");
                        $("#form-message").css("color", "green");
                        $("#loading-spinner").hide();
                        $(".modal-backdrop").hide();
                    } else {
                        console.log('An error occurred while submitting the form.', response.data);
                        $("#form-message").text(
                            "An error occurred while submitting the form. Please refresh the page and try again."
                        );
                        $("#form-message").css("color", "red");
                        $("#loading-spinner").hide();
                        $("#form-message").text(response.data.message);
                        $(".modal-backdrop").hide();
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    $("#form-message").css("color", "red");
                    $("#form-message").text(
                        "An error occurred while submitting the form. Please refresh the page and try again."
                    );
                    $("#loading-spinner").hide();
                    $(".modal-backdrop").hide();
                });
        });

    });
</script>
