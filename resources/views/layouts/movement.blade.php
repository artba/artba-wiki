<article class="documentation_body shortcode_text doc-section" id="goods-movement">
    <div class="shortcode_title">
        <h2>Goods Movement</h2>
        @if (isset($currentDistrict))
            <i>This is state-level data.</i>
            <br><br>
        @endif
        <ul>
            @foreach ($movementsText as $movementText)
                <li>{{ $movementText->text }}</li>
            @endforeach
        </ul>
    </div>
 

    <div class="row">
        <div class="col-md-12 ">
            <div class="float-left" style="width: 50%;">
                <div id="goodsMovementBarChart" style="width: 100%; height: 500px;"></div>
            </div>
            <div class="float-right" style="width: 50%;">
                <div id="goodsMovementPieChart" class="piechart" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
    </div>
    <div class="border_bottom"></div>
</article>
