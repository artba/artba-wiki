 <nav class="navbar navbar-expand-lg menu_one menu_logo_change hidden-print" id="sticky">
     <div class="container">
         <a class="navbar-brand sticky_logo" href="/">
             <img src="https://artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png"
                 srcset="https://artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png 2x" style="max-width:200px;" alt="logo">
             <img src="https://artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png"
                 srcset="https://artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png 2x" style="max-width:200px;" alt="logo">
         </a>
         
         <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
             data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
             aria-label="Toggle navigation">
             <span class="menu_toggle">
                 <span class="hamburger">
                     <span></span>
                     <span></span>
                     <span></span>
                 </span>
                 <span class="hamburger-cross">
                     <span></span>
                     <span></span>
                 </span>
             </span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent">
             <ul class="navbar-nav menu ml-auto dk_menu">
                 <li class="nav-item ">
                     <a class="nav-link" href="/about">
                         About
                     </a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="https://www.artba.org" target="_blank">
                         Visit Artba.org
                     </a>
                 </li>
                 <li class="nav-item hidden-sm hidden-xs hidden-md">
                    <button type="button" class="burger is-closed animated fadeInLeft" data-toggle="offcanvas" style="margin-top: -14px;">
                          <table>
                            <tbody><tr>
                              <td><span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span></td>
                              <td class="hide-grid-toggle"><span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span></td>
                              <td class="hide-grid-toggle-2"><span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span></td>
                            </tr>
                          </tbody></table>
        
                    </button>
                 </li>
             </ul>
         </div>
     </div>
 </nav>
 <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
   <ul class="nav sidebar-nav" id="menu-artba-menu-3"><li class="text-center" style="color:whitesmoke;">Additional Resources <br>
    <hr>
    
</li>
<li>
    <table>
        
        <tbody><tr class="active">
            <td class="text-right">
                <h3>ARTBA</h3>
            </td>
            <td class="text-left"><a href="https://www.artba.org" target="_blank">American Road &amp; Transportation Builders Association</a></td>
        </tr>

                        <tr>
            <td class="text-right">
                <h3>TIAC</h3>
            </td>
            <td class="text-left"><a href="https://transportationinvestment.org" target="_blank">Transportation Investment Advocacy Council</a></td>
        </tr>
                <tr>
            <td class="text-right">
                <h3>SCTPP</h3>
            </td>
            <td class="text-left"><a href="https://artbatdf.org/training_education/safety-certificate/" target="_blank">Safety Certificate for Transportation Project Professionals</a></td>
        </tr>
        <tr>
            <td class="text-right">
                <h3>TDF</h3>
            </td>
            <td class="text-left"><a href="https://www.artbatdf.org" target="_blank">Transportation Development Foundation</a></td>
        </tr>
        <tr>
            <td class="text-right">
                <h3>NEWS</h3>
            </td>
            <td class="text-left"><a href="https://newsline.artba.org" target="_blank">Washington Newsline</a></td>
        </tr>
        <tr>
            <td class="text-right">
                <h3>TB</h3>
            </td>
            <td class="text-left"><a href="https://www.artba.org/news/transportation-builder/" target="_blank">Transportation Builder Magazine</a></td>
        </tr>
    </tbody></table>
</li>
<li><a href="https://artbabridgereport.org/" target="_blank"><img src="https://www.artba.org/wp-content/uploads/2022/02/2022_bridgeReport_verticalBanner.jpg" width="100%"></a></li>
<li><a href="https://www.artba.org" class="text-center"><img src="https://artba.org/wp-content/uploads/2023/08/ARTBA-logo@2x.png" width="100%"></a></li>
</ul>
</nav>
