<article class="documentation_body shortcode_text doc-section" id="safety">
    <div class="shortcode_title">
        <h2>Safety</h2>
        <ul>
            @foreach ($safety as $safeText)
                <li>{{ $safeText->text }}</li>
            @endforeach
        </ul>
    </div>
      <div class="row">
        <div class="col-md-12 text-center">
            <div id="saftyLineChart"></div>
        </div>
    </div>
    <div class="border_bottom"></div>
</article>
