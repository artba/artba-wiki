<ul class="list-unstyled nav-sidebar doc-nav">
    @foreach($sections as $section)
        <li class="nav-item">
            <a href="#{{ strtolower(str_replace(' ','-', $section))}} " class="nav-link">{{ $section }}</a>
        </li>
    @endforeach
</ul>