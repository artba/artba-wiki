   <div class="open_icon" id="right">
       <i class="arrow_carrot-left"></i>
       <i class="arrow_carrot-right"></i>
   </div>
   <div class="doc_rightsidebar">
       @if (!empty($districts))
           <select class="form-control" id="selectDistrict">
               <option value=""> Select District</option>
               @foreach ($districts as $district)
                   <option value="{{ $district->district }}">District: {{ $district->district }}
                   </option>
               @endforeach
           </select>
       @endif
       <br>
       <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#formModal">Download</button>

   </div>
