<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-select.min.css">
    <!-- icon css-->
<link rel="stylesheet" href="/css/eleganticon/style.css">
    <link rel="stylesheet" href="/css/fontawesome/css/all.css"/>

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
    <title>KbDoc</title>
    <script type="text/javascript" src="/js/jsPDF/basic.js"></script>
    <style>
    @media print {
  .hidden-print {
    display: none !important;
  }
}
</style>
</head>

<body class="doc full-width-doc sticky-nav-doc onepage-doc" data-spy="scroll" data-target=".navbar" data-offset="-120">
    <div class="body_wrapper sticky_menu">
        <section class="doc_documentation_area onepage_doc_area" id="sticky_doc">
            <div class="overlay_bg"></div>
            <div class="container-fluid pl-60 pr-60">
                <div class="row doc-container">
                    <div class="col-lg-12 col-md-12">
                        <div class="documentation_info" id="post">
                            <!--doc-->
                            <img src="https://www.artba.org/wp-content/uploads/2014/04/artbalogo1.png" align="center" style="width: 300px; margin:0 auto;">
                            <br>
                            <article class="documentation_body shortcode_text doc-section" id="shortcodes">
                                <div class="shortcode_title">
                                    <h2>Getting Started</h2>
                                    <p><span>Welcome to KbDoc !</span> Get familiar with the Stripe products and explore their features:</p>
                                </div>
                                <p>To use KbDoc WordPress theme you must have WordPress engine installed. We assume you have a working version of WordPress already up and running. We also encourage you to actively use the links below. These useful resources cover most of the general WordPress questions you may have:</p>
                                <ul class="ul">
                                    <li>What is WordPress?&nbsp;<strong><a href="https://en.wikipedia.org/wiki/WordPress" target="_blank" rel="noopener noreferrer">Wikipedia</a></strong> and WordPress FAQ&nbsp;<strong><a href="https://codex.wordpress.org/FAQ_New_To_WordPress" target="_blank" rel="noopener noreferrer">Read here!</a></strong></li>
                                    <li>Tutorial on how to Install WordPress?&nbsp;<strong><a href="https://www.youtube.com/watch?v=ell0SiTZyX8" target="_blank" rel="noopener noreferrer">Watch Video</a></strong></li>
                                    <li>WordPress Lessons&nbsp;<strong><a href="https://codex.wordpress.org/WordPress_Lessons" target="_blank" rel="noopener noreferrer">Visit Articles</a></strong></li>
                                </ul>
                                <h4 class="c_head" id="article">Articles</h4>
                                <br>
                                <table class="table basic_table_info">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <div class="row align-items-center">
                                        <div class="col-sm-5 tour_info_content">
                                            <p>This site is all about <b>getting answers.</b> It's not a discussion forum. There's no chit-chat.</p>
                                            <div class="just_question">
                                                <div class="just_text question_top">
                                                    <p>Just questions....</p>
                                                    <div class="arrow wow fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                                                        <img src="/img/arrow_top.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="just_text answer_bottom">
                                                    <p>Just questions....</p>
                                                    <div class="arrow wow fadeInLeft" data-wow-delay="0.9s" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
                                                        <img src="/img/arrow_bottom.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="tour_preview_img wow fadeInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
                                                <img class="/img-fluid" src="/img/tour_img1.png" alt="">
                                            </div>
                                        </div>
                                    </div>


                                End of demo
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

    <!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/pre-loader.js"> </script>
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="/js/parallaxie.js"></script>
    <script src="/js/TweenMax.min.js"></script>
    <script src="/js/anchor.js"></script>
    <script src="/assets/wow/wow.min.js"></script>
    <script src="/assets/prism/prism.js"></script>
    <script src="/assets/niceselectpicker/jquery.nice-select.min.js"></script>
    <script src="/assets/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/assets/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="/assets/tooltipster/js/tooltipster.bundle.min.js"></script>
    <script src="/assets/font-size/js/rv-jquery-fontsize-2.0.3.js"></script>
    <script src="/assets/video/video.min.js"></script>
    <script src="/assets/video/wistia.js"></script>
    <script src="/js/onpage-menu.js"></script>
    <script src="/js/main.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script>
    window.jsPDF = window.jspdf.jsPDF;
    
    $(function () {
      $("#accordion-basic, #accordion-text, #accordion-graphic, #accordion-font").accordion({
        autoHeight: false,
        navigation: true
      });
      $("#tabs").tabs();
      $(".button").button();
    });



    $('#cmd').click(function () {
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $('#cmd').click(function () {
        doc.html($('#post').html(), 15, 15, {
            'width': 170,
                'elementHandlers': specialElementHandlers
        });
        doc.save('sample-file.pdf');
    });
});
  </script>
</body>
</html>