<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-select.min.css">
    <!-- icon css-->
    <link rel="stylesheet" href="/css/eleganticon/style.css">
    <link rel="stylesheet" href="/assets/niceselectpicker/nice-select.css">
    <link rel="stylesheet" href="/assets/font-size/css/rvfs.css">
    <link rel="stylesheet" href="/assets/animation/animate.css">
    <link rel="stylesheet" href="/assets/video/video-js.css">
    <link rel="stylesheet" href="/assets/tooltipster/css/tooltipster.bundle.css">
    <link rel="stylesheet" href="/assets/prism/prism.css">
    <link rel="stylesheet" href="/assets/prism/prism-coy.css">
    <link rel="stylesheet" href="/assets/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="/assets/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">

    @yield('meta')

    <style>
        @media print {
            .hidden-print {
                display: none !important;
            }

        }

        @if ($export).onepage_doc_area .container-fluid,
        .menu_one .container-fluid {
            max-width: 2000px !important;
        }

        .doc-container .col-md-8 {
            max-width: 2000px !important;
            width: 100% !important;
            flex: 1;
        }

        .doc-section {
            page-break-after: always;
        }

        @endif
    </style>
    @yield('css')
</head>

<body class="doc full-width-doc sticky-nav-doc onepage-doc" data-spy="scroll" data-target=".navbar" data-offset="-120">


    <div class="body_wrapper sticky_menu" id="wrapper">
        <div class="overlay" style="display: none;"></div>
        @include('layouts.nav')

        @yield('content')

        <footer class="simple_footer">
            <img class="leaf_right" src="/img/home_one/leaf_footter.png" alt="">
            <div class="container ">
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <h4>Terms of Use</h4>
                        <p>Any use of ARTBA’s data beyond research and related work must be approved by ARTBA
                            beforehand. All use of ARTBA’s data and reports must include proper credit to "American Road
                            & Transportation Builders Association."</p>
                    </div>
                    <div class="footer_bottom text-center">
                        <div class="container">
                            <p>Copyright © {{ Date('Y') }} <a href="https://www.artba.org">American Road &
                                    Transportation Builders Association</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Tooltip content -->
    <div class="tooltip_templates d-none">
        <div id="tooltipOne" class="tip_content">
            <div class="text">
                <p>Me old mucker bamboozled horse play fantastic skive off baking cakes knees up lurgy spiffing, Harry
                    bog wind up say are you taking the piss porkies hunky-dory, blower pardon you you mug pear shaped
                    pukka get stuffed mate lavatory.</p>
                <h6>Related Reading:<span>Child Theming for Layers</span></h6>
            </div>
        </div>
    </div>
    <div class="tooltip_templates d-none">
        <div id="tooltipTwo" class="tip_content">
            <img src="/img/blog-grid/blog_grid_post1.jpg" alt="">
            <div class="text">
                <p>Me old mucker bamboozled horse play fantastic skive off baking cakes knees up lurgy spiffing, Harry
                    bog wind up say are you taking the piss porkies hunky-dory,</p>
                <h6>Related Reading:<span>Child Theming for Layers</span></h6>
            </div>
        </div>
    </div>
    <div class="tooltip_templates d-none">
        <div id="note-link-a" class="tip_content">
            <div class="text footnotes_item">
                <strong>Footnote Name A</strong>
                <br>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet voluptas dicta dolor.
            </div>
        </div>
    </div>
    <div class="tooltip_templates d-none">
        <div id="note-link-b" class="tip_content">
            <div class="text footnotes_item">
                <strong>Footnote Name B</strong> <a href="#0">[PDF]</a>
                <br>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet voluptas dicta dolor.
            </div>
        </div>
    </div>
    <div class="tooltip_templates d-none">
        <div id="note-link-c" class="tip_content">
            <div class="text footnotes_item">
                <strong>Footnote Name C</strong>
                <br>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet voluptas dicta dolor.
            </div>
        </div>
    </div>
    <video id="vid2" class="video-js vjs-default-skin mfp-hide" preload="auto">
        <p>Video Playback Not Supported</p>
    </video>

    <!-- Back to top button -->
    <a id="back-to-top" title="Back to Top"></a>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="/js/pre-loader.js"></script>
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="/js/parallaxie.js"></script>
    <script src="/js/TweenMax.min.js"></script>
    <script src="/js/anchor.js"></script>
    <script src="/assets/wow/wow.min.js"></script>
    <script src="/assets/prism/prism.js"></script>
    <script src="/assets/niceselectpicker/jquery.nice-select.min.js"></script>
    <script src="/assets/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/assets/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="/assets/tooltipster/js/tooltipster.bundle.min.js"></script>
    <script src="/assets/font-size/js/rv-jquery-fontsize-2.0.3.js"></script>
    <script src="/assets/video/video.min.js"></script>
    <script src="/assets/video/wistia.js"></script>
    <script src="/js/onpage-menu.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="/js/main.js"></script>
    <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-SEK9CXEZEG"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-SEK9CXEZEG'); </script>
    <script type="text/javascript">
        $("div[id^='myModal']").each(function() {

            var currentModal = $(this);

            //click next
            currentModal.find('.btn-next').click(function() {
                currentModal.modal('hide');
                currentModal.closest("div[id^='myModal']").nextAll("div[id^='myModal']").first().modal(
                    'show');
            });

            //click prev
            currentModal.find('.btn-prev').click(function() {
                currentModal.modal('hide');
                currentModal.closest("div[id^='myModal']").prevAll("div[id^='myModal']").first().modal(
                    'show');
            });

        });
        $(function() {
            $("#prospects_form").submit(function(e) {
                $("#loader").hide();
                $("#sent").hide();
                e.preventDefault();
                var pathname = $(location).attr('pathname');
                var email = $("#email").val();
                var subjectLine = "{{ $state->state }}" + "Transportation Facts";
                var bodyText =
                    "Attached you will find a PDF export of transportation facts from our website";
                var filename = "{{ $state->state }}" +
                    "Transportation_Facts.pdf";;
                var pg = "5";
                //Create an Array of selected options
                var selected = $(".sectionSelector:checked").map(function() {
                    return this.value;
                }).get().join(',');

                //open lambda function in new tab based on checked items

                if ($(".sectionSelectorAll:checked").val() == "all") {
                    /* var url =
                        "https://pcmiwqing5kctlaqp52yawtmxq0huszl.lambda-url.us-east-1.on.aws//?url=http://wiki.artba.org" +
                        pathname + "&bodyText=" + bodyText + "&email=" +
                        email + "&filename=" + filename + "&subjectLine=" + subjectLine; */
                    var url =
                        "https://pcmiwqing5kctlaqp52yawtmxq0huszl.lambda-url.us-east-1.on.aws/?subjectLine=" +
                        subjectLine + "&html=" + bodyText + "&filename=" + filename + "&url=" +
                        "http://wiki.artba.org" + pathname + "&email=" + email;
                }

                if (selected.length > 0) {
                    /* var url =
                        "https://pcmiwqing5kctlaqp52yawtmxq0huszl.lambda-url.us-east-1.on.aws//?url=http://wiki.artba.org" +
                        pathname + "&pg=" + selected + "&bodyText=" + bodyText + "&email=" +
                        email + "&filename=" + filename + "&subjectLine=" +
                        subjectLine; */

                    var url =
                        "https://pcmiwqing5kctlaqp52yawtmxq0huszl.lambda-url.us-east-1.on.aws/?subjectLine=" +
                        subjectLine + "&html=" + bodyText + "&filename=" + filename + "&url=" +
                        "http://wiki.artba.org" + pathname + "&email=" + email + "&pg=" + selected;


                }
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    beforeSend: function() {
                        // Show image container
                        $("#loader").show();
                    },

                    beforeSend: function() {
                        // Show image container
                        $("#loader").show();
                    },
                    success: function(response) {
                        $("#sent").show();

                        /* $('.response').empty();
                        $('.response').append("Email has been sent!"); */
                    },
                    /* error: function(response) {
                        $("#loader").show();

                    }, */
                    complete: function(data) {
                        // Hide image container
                        $("#loader").hide();
                        $("#sent").show();

                    }

                });
                /* $("#prospects_form").hide(); */

            });

        });


        $(".sectionSelector").click(function() {
            var sectionSelectorValue = $(this).val();
            if (sectionSelectorValue != "all") {
                $("input.sectionSelectorAll").prop('checked', false);
            }
        });

        $(".sectionSelectorAll").click(function() {
            $(".sectionSelector").each(function() {
                $(this).prop('checked', false);
            });
        });

        $("#selectDistrict").on("change", function() {
            var district = $(this).val();
            if (district != "") {
                window.location.href = "/state/{{ $state->state_abbr }}/district/" + district
            }
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var trigger = $('.burger'),
                overlay = $('.overlay'),
                isClosed = false;

            trigger.click(function() {
                burger_cross();
                $('#wrapper').toggleClass('toggled');
            });

            function burger_cross() {
                if (isClosed == true) {
                    overlay.hide();
                    trigger.removeClass('is-open');
                    trigger.addClass('is-closed');
                    isClosed = false;

                } else {
                    overlay.show();
                    trigger.removeClass('is-closed');
                    trigger.addClass('is-open');
                    isClosed = true;
                }
            }

            $(".burger").click(function() {
                $('#wrapper').toggleClass('toggled');
            });

            $('[data-toggle="offcanvas"]').click(function() {
                $('#wrapper').toggleClass('toggled');
            });

            $('body').on("click", ".navbar-toggle", function() {
                $(".primarymenu").toggleClass('collapse');
                console.log("show");
            });

            $.get("https://www.artba.org/wp-content/themes/Avada/home/shelf.php?site=bridge", function(data) {
                $("#menu-artba-menu-3").html(data);
            });
        });
    </script>
    @yield('js')
</body>

</html>
