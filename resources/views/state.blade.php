@extends('layouts.wiki')
@section('meta')
    <title>{{ $state->state }} - Your State's Infrastructure At-A-Glance</title>
    <meta property="og:url" content="https://transpoinfo.org/state/{{ $state->state_abbr }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $state->state }} - Your State's Infrastructure At-A-Glance" />
    <meta property="og:description" content="Key facts about your state’s roads, bridges, waterways and other transportation infrastructure." />
    <meta property="og:image" content="https://transpoinfo.org/img/socialbanner.jpeg" />
@endsection
@section('css')
    <style type="text/css">
        td.indent {
            padding-left: 30px;
        }

        tr.category {}

        .doc-section .row .col-md-6 {
            max-width: 435px;
        }

        @media (max-width:750px){
            .float-left, .float-right{
                width: 100% !important;
            }
            .piechart{
                max-height: 350px !important;
                padding-top: 50px;
                border-top: 1px solid rgba(0,0,0,0.1);
                padding-bottom: 20px;
            }
        }

    </style>
@endsection

@section('content')
    <section class="doc_documentation_area onepage_doc_area" id="sticky_doc">
        <div class="overlay_bg"></div>
        <div class="container-fluid pl-60 pr-60">
            <div class="row doc-container">
                <div class="col-lg-2 doc_mobile_menu doc-sidebar display_none hidden-print">
                    <aside class="doc_left_sidebarlist">
                        <h3 class="nav_title">Navigation</h3>
                        <div class="scroll">
                            @include('layouts.side-nav')
                        </div>
                    </aside>
                </div>
                <div class="col-lg-8 col-md-8">
                    <div class="documentation_info" id="post">
                        <!--doc-->
                        <article class="documentation_body doc-section pt-0" id="infrastructure-network">
                            <div class="shortcode_title">
                                <h2 style="font-size: 50px">{{ $state->state }} Transportation Facts</h2>
                                {{-- <p>The design, construction and maintenance of transportation infrastructure in
                                    {{ $state->state }}</p> --}}

                                <table>

                                </table>

                            </div>
                            @include('layouts.infrastructure-network')
                            <div class="border_bottom"></div>
                        </article>
                        @include('layouts.economic-impacts')
                        @include('layouts.mobility')
                        @include('layouts.movement')
                        @include('layouts.safety')
                        @include('layouts.highway-funding')
                        @include('layouts.transit')
                        @include('layouts.airport')
                        @include('layouts.information')
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 doc_right_mobile_menu">
                    @include('layouts.side-right')
                </div>
            </div>
        </div>
    </section>
    @include('layouts.modals.export')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    @include('layouts.charts.goodsMovementBar', ['points' => $movementsChartTwo])
    @include('layouts.charts.goodsMovementPie', ['points' => $movementsChartOne])
    @include('layouts.charts.econPie1', ['points' => $economicsChartTwo])
    @include('layouts.charts.econPie2', ['points' => $economicsChartOne])
    @include('layouts.charts.mobilityBar', ['points' => $mobilityChartTwo])
    @include('layouts.charts.mobilityPie', ['points' => $mobilityChartOne])
    @include('layouts.charts.highwayPie1', ['points' => $highwayFunding['graph1']])
    @include('layouts.charts.highwayPie2', ['points' => $highwayFunding['graph2']])
    @include('layouts.charts.highwayPie3', ['points' => $highwayFunding['graph3']])
    @include('layouts.charts.highwayPie4', ['points' => $highwayFunding['graph4']])

    @include('layouts.charts.transitPie1', ['points' => $transit['graph1']])
    @include('layouts.charts.transitPie2', ['points' => $transit['graph2']])
    @include('layouts.charts.safetyLine', ['points' => $safetyGraph])

    @include('layouts.charts.airportPie1', ['points' => $airportsFunding['graph1']])
    @include('layouts.charts.airportPie2', ['points' => $airportsFunding['graph2']])
@endsection('js')
