@extends('layouts.master')

@section('header')
    <title>TranspoInfo - America's Infrastructure At-A-Glance</title>
    <meta property="og:url" content="https://transpoinfo.org/" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="TranspoInfo - America's Infrastructure At-A-Glance" />
    <meta property="og:description" content="Key facts about each state's roads, bridges, waterways and other transportation infrastructure." />
    <meta property="og:image" content="https://transpoinfo.org/img/socialbanner.jpeg" />
    <link href="/Map/map.css" rel="stylesheet">
    <style type="text/css">
        .kbDoc-banner-support {
            padding: 100px 0;
            background-color: rgb(255, 255, 255) !important;
            background-image: none !important;
        }
        path,text{
            cursor: pointer;
        }
        .banner-description{
            padding-left: 20px;
            padding-right: 20px;
        }
        path:hover{
            background-color: #00507b;
            fill: rgb(0,80,123);
            transition: 0.3s;
        }
    </style>
@endsection

@section('content')
<section class="kbDoc-banner-support" style="height: inherit !important; padding-bottom: 0; margin-top: 2em;">
            <div class="banner-content text-center">
                <div class="banner-content-wrapper">
                    <h1 class="banner-title wow fadeInUp">America's Infrastructure At-A-Glance</h1>
                    <p class="banner-description wow fadeInUp text-left" data-wow-delay="0.3s">
                        <br>
                        America's network of roads, bridge, rail, and public transportation that drives the economy is the result of a long-standing partnership between state and federal entities. Decades of investment from federal, state and local sources created a network to move people and goods safely and efficiently across the country. Now, thanks to the IIJA, the network is seeing long overdue investment to rebuild and modernize the way people and freight travel.
                    </p>
                    <p class="banner-description wow fadeInUp text-left" data-wow-delay="0.3s">
                        Click on the map below to find facts about your state and congressional district's road miles, bridges, commuting patterns, waterways, transit, and more – all at your fingertips.  
                    </p>


                    <select class="form-control banner-description" id="state-selector" style="margin-bottom: 25px; max-width:600px; margin-left:auto; margin-right: auto;">
                        <option value="">Select State</option>
                        @foreach ($states as $state)
                            <option value="{{ $state->state_abbr }}">{{ $state->state }}</option>
                        @endforeach
                    </select>
                    <div class="banner-search-form-wrapper wow fadeInUp" data-wow-delay="0.5s" style="max-width: 780px; margin: 0 auto;">
                        <!-- start Fla-shop.com HTML5 Map -->


                        <svg id="my_dataviz" width="720" height="490"></svg>


                        {{-- <a href="/state/WV"><img src="https://artbabridgereport.org/img/state-map.jpg"
                                style="width: 100%; max-width: 600px;"></a> --}}
                    </div>
                    <!-- /.banner-search-form-wrapper -->
                </div>
                <!-- /.banner-content-wrapper -->

            </div>
            <!-- /.banner-content -->

            {{-- <div class="bottom-shape">
                <img src="/img/home_support/banner-bottom-shape.png" alt="banner">
            </div> --}}
        </section>
        <!-- /.kbDoc-banner-support -->
        <section class="doc_faq_area_two sec_pad" style="padding-top: 0;">
            <div class="container">
                <div class="section_title text-center">
                    <h2 class="h_title wow fadeInUp">National Transportation Statistics:</h2>
                </div>
                <div class="row">
                    <ul style="display:block;" class="banner-description">
                        <li><strong>Over $650 billion in economic activity</strong> is generated annually from construction, design, and maintenance work performed on transportation projects, which creates and supports <strong>4.4 million jobs</strong>.</li>
                        <li>The Federal Highway Administration estimates that every <strong>$1 billion in highway and bridge infrastructure investment supports at least 13,000 jobs</strong> throughout the U.S. economy. This includes work in retail, manufacturing, transportation and warehousing, food services and other industries.</li>
                        <li>The 2021 <strong>infrastructure law</strong> contains <strong>$450 billion</strong> for highway, bridge, safety and public transportation system improvements.</li>
                        <li></li>
                            The U.S. transportation network includes:
                            <ul>
                                <li><strong>4.19 million miles of road</strong>, including Alaska and Hawaii.</li>
                                <li><strong>48,756 miles of Interstate</strong> Highways.</li>
                                <li><strong>619,588 bridges</strong> of which 43,578 are classified as structurally deficient.</li>
                            </ul>
                        </li>
                        <li>Interstates comprise just over <strong>1 percent of highway mileage, but carry over 25 percent of all highway traffic.</strong>
                            <ul>
                                <li>The National Highway System is made up of the Interstates and 171,759 miles of other major roads. This is the core of the nation's roadway network.</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
@endsection

@section('js')
<script src="https://d3js.org/d3.v4.js"></script>
<script src="https://d3js.org/d3-scale-chromatic.v1.min.js"></script>
<script src="https://d3js.org/d3-geo-projection.v2.min.js"></script>
    <script>

//
var screenWidth = $( window ).width();
var svgWidth = $("#my_dataviz").attr("width");
var svgHeight = $("#my_dataviz").attr("height");
var svgScale = 540;
var svgX = 1320;
var svgY = 648;
var svgReducer = 0.5;

if(screenWidth < 600){
    svgHeight = svgHeight * svgReducer;
    svgWidth = svgWidth * svgReducer;
    svgScale = svgScale * svgReducer;
    svgX = svgX * svgReducer * 1.04;
    svgY = svgY * svgReducer;
    $("#my_dataviz").attr("height", svgHeight);
}

// The svg
var svg = d3.select("svg"),
    width = svgWidth
    height = svgHeight

// Map and projection
var projection = d3.geoMercator()
    .scale(svgScale) // This is the zoom
    .translate([svgX, svgY]); // You have to play with these values to center your map

// Path generator
var path = d3.geoPath()
    .projection(projection)

// Load external data and boot
d3.json("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/us_states_hexgrid.geojson.json", function(data){

  // Draw the map
  svg.append("g")
      .selectAll("path")
      .data(data.features)
      .enter()
      .append("path")
          .attr("fill", "#0077b4")
          .attr("d", path)
          .attr("stroke", "white")
          .on("click", function(d) { window.open("https://transpoinfo.org/state/" + d.properties.iso3166_2 ); }) 

  // Add the labels
  svg.append("g")
      .selectAll("labels")
      .data(data.features)
      .enter()

      .append("text")

        .attr("x", function(d){return path.centroid(d)[0]})
        .attr("y", function(d){return path.centroid(d)[1]})
        .text(function(d){ return d.properties.iso3166_2})
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .style("font-size", 11)
        .style("fill", "white")
        .on("click", function(d) { window.open("https://transpoinfo.org/state/" + d.properties.iso3166_2 ); })
      

});

var stateSelector = document.getElementById("state-selector");

stateSelector.addEventListener("change", function(ev) {
            var stateName = ev.target.value;
            if (stateName) {
                window.location.href = "/state/" + stateName;
            }
        });
        /*
        var map = new FlaMap(map_cfg);
        map.drawOnDomReady('map-container');
        var stateSelector = document.getElementById("state-selector");

        //populate the dropdown with state options
        for (var sid in map_cfg.map_data) {
            var state = map_cfg.map_data[sid];
            var option = document.createElement("option");
            option.value = state.shortname;
            option.text = state.name;
            stateSelector.add(option);
        }

        
        map.on('click', function(ev, sid, map) {

            var state = map_cfg.map_data[sid];
            var stateName = state.shortname;
            console.log(stateName);
            window.location.href = "/state/" + stateName;
            //open in a new tab
            window.location.href = "/state/" + stateName;


        });
        */
    </script>
@endsection