<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');

Route::get('/state/{state}', 'StateController@index')->name('state');
Route::get('/state/{state}/district/{district}', 'StateController@district')->name('district');

Route::get('html-pdf', 'HTMLPDFController@htmlPdf')->name('htmlPdf');